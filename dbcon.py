#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sqlite3


class DBconnection:
    def __init__(self, tbl_name, db_path, logger):
        self.tb_name = tbl_name
        self.lgr = logger.getChild('dbcon')
        self.__connect__(db_path)
        self.status = False
        self.db_path = db_path
        self.sql = ''

    def getSql(self):
        return self.sql

    def __connect__(self, db_path):
        try:
            self.con = sqlite3.connect(db_path)
        except:
            self.lgr.error('connection error %s', db_path)
            exit(0)
        self.c = self.con.cursor()
        self.status = True
        self.c.execute('PRAGMA synchronous = OFF')
        self.commit()

    def disconnect(self):
        if self.status:
            self.con.close()
            self.status = False

    def createDB(self, name_db, sql_schema):
        if not self.status:
            self.__connect__(self.db_path)
        self.sql = ''.join(sql_schema)
        try:
            self.con.executescript(self.sql)
        except:
            self.lgr.debug('SQL: %s', self.sql)

    def insertDictIntoTb(self, dict_items):
        columns = ', '.join(list(dict_items.keys()))
        values = list(dict_items.values())
        placeholders = ','.join('?' * len(dict_items))
        self.sql = 'INSERT into ' + self.tb_name + \
            ' ( {} ) VALUES ( {} )'.format(columns, placeholders)
        try:
            self.c.execute(self.sql, values)
        except sqlite3.Error as msg:
            print(msg.args)
            self.lgr.error('SQL: %s', self.sql)
            exit(0)

    def commit(self):
        self.con.commit()




